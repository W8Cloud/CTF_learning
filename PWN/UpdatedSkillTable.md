# Skill Table of PWN
## Common Logic
- shellcraft
- falsify GOT
- falsify returned value
- hijack function
- hijack register
## In stack
- Stackoverflow-Ret2lib
- Stackoverflow-Ret2reg
- Stackoverflow-nolibLeak
- Stackoverflow-StackSmash
- FmtStrVul-canaryleak
- FmtStrVul-MEMW
## In heap
- FastbinAttack-2Free-HijackFunc
- FastbinAttack-2Free-FakeList